import React, {Component} from 'react';
import SafeViewArea from 'react-native';

class Home extends Component {
    render() {
        return this.props.children;
    }
}

export default Home;