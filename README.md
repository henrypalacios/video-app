# React Native App (VideoApp)
- Testing the powerful of Javascript for create native mobile apps  
through React and managment a global state with Redux.

## Preview  
![](./doc/preview.gif)

## Instalation  
- requirements:  
    node.js  
    expo V35

- run:  
    `npm install`  
    `expo start`
